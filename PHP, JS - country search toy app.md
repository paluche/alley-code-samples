# Toy app for searching restcountries.eu

https://gitlab.com/paluche/country-search/

This is a toy web app I was tasked with building by another company I'm interviewing with. The challenge was to build a web app that allows the user to search the restcountries.eu service for countries by name or their 2/3 character alpha code. I was required to submit the search term to a backend PHP endpoint that I built via JS, and then from PHP, carry out the appropriate REST queries, then return the results in JSON.

The frontend code is unremarkable, but the backend I am fairly proud of, considering the time constraint I was under, and how much new ground I covered while developing it. In your required qualifications, you say that I should "demonstrate that [I] can learn a new language, technology stack, or framework quickly and on demand". Perhaps this is a good example? This was my first time using all of the dozen or so libraries I used in this project, as well as the dependency injection pattern. I also tried to code to an interface rather than an implementation, such that the SearchController isn't coupled to the RestCountriesEu class that actually carries out the search, but I got stuck on filtering the results in an abstract way.
