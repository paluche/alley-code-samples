(function animateMobileMenu() {

    /* The wrapper div that will take the CSS transforms */
    var transformWrap = document.getElementById("header-transform-wrap");

    /* The toggle button to open the menu */
    var toggle = document.getElementById("mobile-menu-toggle");

    /* We need to create a closure on the fly whenever the user opens the mobile
       menu, but the removeEventListener callback needs it to be scoped here,
       otherwise it gets redefined every time. */
    var disableScrollFunction;

    /**
     * Toggle Mobile Menu
     *
     *
     */
    function toggleMobileMenu() {
        if (this.classList.contains('open')) {
            window.removeEventListener('scroll', disableScrollFunction);
        } else {
            let x = window.scrollX;
            let y = window.scrollY;
            disableScrollFunction = function disableScrollFunction() {
                window.scrollTo(x,y);
            };
            window.addEventListener('scroll', disableScrollFunction);
        }
        this.classList.toggle('open');
    };

    /* Get a px equivalent of 1% of the inner height of the window, excluding
       browser chrome. Assign this value to a custom property on the document. */
    function recalculateVH() {
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    }


    /* Calculate the inner height of the window, sans browser chrome, initially
       and on resize, which triggers when the URL bar hides and shows */
    recalculateVH();
    window.addEventListener('resize', recalculateVH );


    toggle.onclick = function () {toggleMobileMenu.call(transformWrap)};


}());