# "Full height" mobile dropdown menu

This is not super recent code, but I tried to recount my thought process from memory. I wanted to build a custom mobile navigation menu that pulls down to the bottom of your screen to reveal a scrollable list of menu items. (See FigA and FigB) I wanted to do this using as little JS as possible, without any libraries, and without transpiling.

I knew that unless I used [the checkbox hack][1], I would have to at least use JS to toggle a class on the element to indicate a state change, so I did that, toggling a class that set the height of my dropdown to 100vh.

That's when I learned that different mobile browsers have different ideas of [what 100vh should be][2], but it was trending towards being equal to the actual screen height, unlike how vh units behave on desktop. Following this [CSS tricks guide][2], I added a function to define a custom CSS property to represent the inner height of the window, and then used that value as the target height of the "open" class.

That worked ok, but then I noticed that while scrolling through the menu items, the page in the background would scroll, leaving you in a different place after closing the menu, which is poor UX.

Googling around for solutions, I was surprised to find that there wasn't a tidy way to disable scroll events, so I used the one that was most palatable, which was just a scroll event listener, which isn't even that palatable, as it calls my function on every scroll event, which is gross. I think I was planning on letting myself use underscore and to `_.debounce()` the function.

Looking at this again, I realize it's silly. Can't I just skip the scroll event listeners and just record the x/y coordinates on open, let the body scoll wherever it wants while the menu is open, and then restore the scroll position on close?

[1]: https://css-tricks.com/the-checkbox-hack/

[2]: https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
