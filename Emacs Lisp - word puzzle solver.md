# Optimal word puzzle solver in Emacs Lisp, functional paradigm

https://gitlab.com/paluche/computer-whiz/blob/master/computer-whiz.el

This is a solver for the terminal hacking mini-game in the Fallout 3 and 4 video games, based on the 1970 board game "Mastermind". The object of the game is to determine which word from a list of words is the correct password. The player is given a number of chances to guess the correct word, and upon guessing incorrectly, is told the number of characters that are in the correct position.

For example, if the correct password is "CONTACT", and the player guessed "TACTILE", the game will inform you that 1/7 of the letters match, since even though both words have at least two Ts, one A, and one C, only the 4th letter of each word directly matches.

This program determines the shortest sequence of words you must guess to determine the answer. I'm proud of it because it demonstrates my ability to pull a problem apart into smaller chunks, most of which are solved via pure functions. It demonstrates a strong command of common functional programming concepts and tools such as recursion, closures, and higher order functions.
